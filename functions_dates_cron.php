#!/usr/local/bin/php 
<?php
/**
 * functions_dates_cron.php
 *
 * @package Dates Reminder Module
 * @copyright Copyright 2007-2008 Numinix Technology http://www.numinix.com
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: functions_dates_cron.php, v 1.01 01.18.2008 17:13:59 numinix $
 * @author Numinix Technology
 */

  require('includes/application_top.php');
  
  function zen_check_birthday() {
    global $db;
    $days_from_databas = NUMINIX_DATES_MOD_REMINDER_DATE;
		$customers_check_birthday_query = "SELECT dates_customers_id, dates_date, dates_first_name, dates_last_name, dates_email_address
	             FROM " . TABLE_DATES . " order by dates_customers_id ASC";
		$customers_check_birthday_result = $db->Execute($customers_check_birthday_query);
		$ok='';
		while (!$customers_check_birthday_result->EOF) {
			if($customers_check_birthday_result->fields['dates_date']){
			
				$raw_date = $customers_check_birthday_result->fields['dates_date'];
			
				$birth_year = substr($raw_date, 0, 4);
				$birth_month = (int)substr($raw_date, 5, 2);
				$birth_day = (int)substr($raw_date, 8, 2)-$days_from_databas;
				$birth_hour = (int)substr($raw_date, 11, 2);
				$birth_minute = (int)substr($raw_date, 14, 2);
				$birth_second = (int)substr($raw_date, 17, 2);
				
				$today_day = date("d");
				$today_month = date("m");
				$today_year = date("Y");
				$today_hour = date("h");
				$today_minute = date("m");
				$today_second = date("s");
		
        if ($today_year != 1969 && @date('Y', mktime($today_hour, $today_minute, $today_second, $today_month, $today_day, $today_year)) == $today_year) {
        	$today_date =  date('m/d', mktime($today_hour, $today_minute, $today_second, $today_month, $today_day, $today_year));
        } else {
        	$today_date =  ereg_replace('2037' . '$', $today_year, date('m/d', mktime($today_hour, $today_minute, $today_second, $today_month, $today_day, 2037)));
    		}

        if ($today_year != 1969 && @date('Y', mktime($birth_hour, $birth_minute, $birth_second, $birth_month, $birth_day, $today_year)) == $today_year) {
        	$birth_date =  date('m/d', mktime($birth_hour, $birth_minute, $birth_second, $birth_month, $birth_day, $today_year));
        } else {
        	$birth_date =  ereg_replace('2037' . '$', $today_year, date('m/d', mktime($birth_hour, $birth_minute, $birth_second, $birth_month, $birth_day, 2037)));
    		}
			
		
			}
			if($today_date==$birth_date){
			
				$recipient_query = "SELECT customers_id, customers_firstname, customers_lastname
														FROM " . TABLE_CUSTOMERS . " 
														WHERE customers_id = " . $customers_check_birthday_result->fields['dates_customers_id'] . "
														LIMIT 1";
				$recipient = $db->Execute($recipient_query);
									
				
				//$email_temp_1 = sprintf(NUMINIX_DATES_MOD_REMINDER_TEMPLATE_1, $customers_check_birthday_result->fields['dates_first_name'] . ' ' . $customers_check_birthday_result->fields['dates_last_name']);
				$email_temp_1 = sprintf(NUMINIX_DATES_MOD_REMINDER_TEMPLATE_1, $recipient->fields['customers_firstname'] . ' ' . $recipient->fields['customers_lastname']);
				$email_temp_2 = sprintf(NUMINIX_DATES_MOD_REMINDER_TEMPLATE_2, $customers_check_birthday_result->fields['dates_first_name'] . ' ' . $customers_check_birthday_result->fields['dates_last_name'] . ' ' . zen_date_short($customers_check_birthday_result->fields['dates_date']));
				$email_temp_3 = NUMINIX_DATES_MOD_REMINDER_TEMPLATE_3;
				$email_temp_4 = NUMINIX_DATES_MOD_REMINDER_TEMPLATE_4;
			
				$email_text = $email_temp_1 . $email_temp_2 .  $email_temp_3 . $email_temp_4;
				$html_msg['EMAIL_MESSAGE_HTML'] = $email_temp_1 . '<BR><BR>' . $email_temp_2 .  '<BR><BR>' . $email_temp_3 . '<BR><BR>' . $email_temp_4;
				
			    zen_mail($recipient->fields['customers_firstname'] . ' ' . $recipient->fields['customers_lastname'], $customers_check_birthday_result->fields['dates_email_address'], DATES_EMAIL_TITLE, $email_text, STORE_NAME, EMAIL_FROM, $html_msg, 'dates_reminder');
			} else {
			}
		  	$customers_check_birthday_result->MoveNext();
			
		}
	//      return $send;	
}
    
  zen_check_birthday();
  
  require('includes/application_bottom.php');
?>
