<?php
/**
 *
 * @package dates reminder module
 * @copyright Copyright 2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account_dates.php v1.01 2008-01-19 16:05:59Z numinix $
 */

define('ADD_DATES', 'Dates reminder'); // Link name
define('DATES_DATE', 'Important Date:'); // Change to Birthday, Anniversary, etc.
define('DATES_DATE_NAME', 'Name'); // For stored reminders
define('DATES_DATE_DELETE', 'Delete'); // To remove stored reminders
define('DATES_EMAIL_TITLE', 'Dates reminder'); // Displayed above NUMINIX_DATES_MOD_REMINDER_TEMPLATE_1
define('ENTRY_DATE_OF_REMIND_ERROR', 'Is your remind date correct? Our system requires the date in this format: MM/DD/YYYY (eg 05/21/1970)');
define('REMIND_NOTE', 'Change this text in includes/languages/english/extra_definitions/account_dates.php'); // The message sent to customers regarding what the reminder is for
define('NUMINIX_DATES_MOD_REMINDER_TEMPLATE_1', 'Dear %s '. "\n\n" .''); // Dear customers_firstname
define('NUMINIX_DATES_MOD_REMINDER_TEMPLATE_2', 'We are sending you this email as you have requested to receive the following reminder: %s'. "\n\n" .'');
define('NUMINIX_DATES_MOD_REMINDER_TEMPLATE_3', ''. REMIND_NOTE . "\n\n" .'');
define('NUMINIX_DATES_MOD_REMINDER_TEMPLATE_4',  '' . STORE_NAME . "\n\n" .'');
?>
