<?php
/**
 * @package dates reminder module
 * @copyright Copyright 2006-2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account_dates.php v1.01 2008-01-19 16:23:59Z numinix $
 */

define('NAVBAR_TITLE', 'Date reminder');
define('HEADING_TITLE', 'Date reminder');
define('NEW_DATE_REMINDER', 'New Date reminder');
?>
