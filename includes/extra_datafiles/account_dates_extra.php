<?php
/**
 * @package dates reminder module
 * @copyright Copyright 2006-2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: accounts_dates_extra.php v1.01 2008-01-19 16:21:59Z numinix $
 */


define('TABLE_DATES', DB_PREFIX . 'dates');
define('FILENAME_ACCOUNT_DATES', 'account_dates');
define('FILENAME_DEFINE_DATES', 'define_dates_reminder');

?>
