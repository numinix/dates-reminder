<?php
/**
 * Header code file for the customer's Account-Edit page
 *
 * @package dates reminder module
 * @copyright Copyright 2006-2008 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: header_php.php v1.01 2008-01-19 16:26:59Z numinix $
 */
// This should be first line of the script:
$zco_notifier->notify('NOTIFY_HEADER_START_ACCOUNT_EDIT');

$define_page = zen_get_file_directory(DIR_WS_LANGUAGES . $_SESSION['language'] . '/html_includes/', FILENAME_DEFINE_DATES, 'false');

if (!$_SESSION['customer_id']) {
  $_SESSION['navigation']->set_snapshot();
  zen_redirect(zen_href_link(FILENAME_LOGIN, '', 'SSL'));
}
require(DIR_WS_MODULES . zen_get_module_directory('require_languages.php'));

if($_GET['action']=='delete'){
  $sql = "DELETE FROM " . TABLE_DATES . "
          WHERE  dates_id = :delete";
  $sql = $db->bindVars($sql, ':delete', $_GET['id'], 'integer');
  $db->Execute($sql);

  $messageStack->add_session('account_dates', 'Date has been deleted', 'success');

   zen_redirect(zen_href_link(FILENAME_ACCOUNT_DATES, '', 'SSL'));
}

if (isset($_POST['action']) && ($_POST['action'] == 'process')) {

  $dates_first_name = zen_db_prepare_input($_POST['dates_first_name']);
  $dates_last_name = zen_db_prepare_input($_POST['dates_last_name']);
  $dates_date = (empty($_POST['dates_date']) ? zen_db_prepare_input('0001-01-01 00:00:00') : zen_db_prepare_input($_POST['dates_date']));
  $dates_email_address = zen_db_prepare_input($_POST['dates_email_address']);
  $dates_customers_id = zen_db_prepare_input($_POST['customer_id']);
  
  $error = false;

  if (strlen($dates_first_name) < ENTRY_FIRST_NAME_MIN_LENGTH) {
    $error = true;
    $messageStack->add('account_dates', ENTRY_FIRST_NAME_ERROR);
  }

  if (strlen($dates_last_name) < ENTRY_LAST_NAME_MIN_LENGTH) {
    $error = true;
    $messageStack->add('account_dates', ENTRY_LAST_NAME_ERROR);
  }


    if (ENTRY_DOB_MIN_LENGTH > 0 or !empty($_POST['dates_date'])) {
      if (substr_count($dob,'/') > 2 || checkdate((int)substr(zen_date_raw($dates_date), 4, 2), (int)substr(zen_date_raw($dates_date), 6, 2), (int)substr(zen_date_raw($dates_date), 0, 4)) == false) {
        $error = true;
        $messageStack->add('account_dates', ENTRY_DATE_OF_REMIND_ERROR);
      }
    }

  if (strlen($dates_email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
    $error = true;
    $messageStack->add('account_dates', ENTRY_EMAIL_ADDRESS_ERROR);
  }

  if (!zen_validate_email($dates_email_address)) {
    $error = true;
    $messageStack->add('account_dates', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
  }


  if ($error == false) {
    $sql_data_array = array('dates_customers_id' => $dates_customers_id,
                            'dates_first_name' => $dates_first_name,
                            'dates_last_name' => $dates_last_name,
                            'dates_date' => zen_date_raw($dates_date),
                            'dates_email_address' => $dates_email_address);
 
    zen_db_perform(TABLE_DATES, $sql_data_array);

    $zco_notifier->notify('NOTIFY_HEADER_ACCOUNT_EDIT_UPDATES_COMPLETE');

    $messageStack->add_session('account_dates', 'Date has beed added.', 'success');

    zen_redirect(zen_href_link(FILENAME_ACCOUNT_DATES, '', 'SSL'));
  }
}

$account_query = "SELECT dates_id, dates_first_name, dates_last_name,
                         dates_date, dates_email_address
                  FROM   " . TABLE_DATES . "
                  WHERE  dates_customers_id = :customersID order by dates_date ASC";

$account_query = $db->bindVars($account_query, ':customersID', $_SESSION['customer_id'], 'integer');
$account = $db->Execute($account_query);

$account_query2 = "SELECT customers_gender, customers_firstname, customers_lastname,
                         customers_dob, customers_email_address, customers_telephone,
                         customers_fax, customers_email_format, customers_referral
                  FROM   " . TABLE_CUSTOMERS . "
                  WHERE  customers_id = :customersID";

$account_query2 = $db->bindVars($account_query2, ':customersID', $_SESSION['customer_id'], 'integer');
$account2 = $db->Execute($account_query2);

$breadcrumb->add(NAVBAR_TITLE_1, zen_href_link(FILENAME_ACCOUNT_DATES, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_2);

// This should be last line of the script:
$zco_notifier->notify('NOTIFY_HEADER_END_ACCOUNT_EDIT');
?>
