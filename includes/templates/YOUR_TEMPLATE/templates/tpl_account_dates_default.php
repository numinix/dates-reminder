<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=account_edit.<br />
 * View or change Customer Account Information
 *
 * @package templateSystem
 * @copyright Copyright 2006-2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2005 Zen Cart Development Team
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_account_dates_default.php v1.01 2008-01-19 16:27:59Z numinix $
 * @copyright Portions Copyright 2003 osCommerce
 */
?>
<div class="centerColumn" id="accountEditDefault">
<?php echo zen_draw_form('account_dates', zen_href_link(FILENAME_ACCOUNT_DATES, '', 'SSL'), 'post', 'onsubmit="return check_form(account_dates);"') . zen_draw_hidden_field('action', 'process'); ?>
<h1 id="dateHeading"><?php echo HEADING_TITLE; ?></h1>

<?php
/**
 * get the Define Date Reminder Text
 */
?>
<div id="indexDefaultMainContent" class="content"><?php require($define_page); ?></div>

<?php if ($messageStack->size('account_dates') > 0) echo $messageStack->output('account_dates') . '<BR><BR>'; ?>

<?php 
if($account->fields['dates_id']){
?>
<fieldset>
<legend><?php echo HEADING_TITLE; ?></legend>
<?php
while (!$account->EOF) {
$dates_id = $account->fields['dates_id'];
echo $table = '
<div>
'. DATES_DATE_NAME .': '. $account->fields['dates_first_name'] .' '. $account->fields['dates_last_name'] .'&nbsp;|&nbsp;
'. DATES_DATE .' '. zen_date_short($account->fields['dates_date']) .'&nbsp;|&nbsp;
<a href="'. zen_href_link(FILENAME_ACCOUNT_DATES, 'action=delete&id='. $dates_id . '', 'SSL') .'">'. DATES_DATE_DELETE .'</a>
<BR><BR>
</div>';  
   $account->MoveNext();
   }
   }
?>
</fieldset>
<fieldset>
<legend><?php echo NEW_DATE_REMINDER; ?></legend>
<div class="alert forward"><?php echo FORM_REQUIRED_INFORMATION; ?></div>
<br class="clearBoth" />

<label class="inputLabel" for="firstname"><?php echo ENTRY_FIRST_NAME; ?></label>
<?php echo zen_draw_input_field('dates_first_name', $account2->fields['customers_firstname'], 'id="dates_first_name"') . (zen_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="alert">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?>
<br class="clearBoth" />

<label class="inputLabel" for="lastname"><?php echo ENTRY_LAST_NAME; ?></label>
<?php echo zen_draw_input_field('dates_last_name', $account2->fields['customers_lastname'], 'id="dates_last_name"') . (zen_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="alert">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?>
<br class="clearBoth" />

<label class="inputLabel" for="dob"><?php echo DATES_DATE; ?></label>
<?php echo zen_draw_input_field('dates_date', zen_date_short($account2->fields['customers_dob']), 'id="dates_date"') . (zen_not_null(ENTRY_DATE_OF_BIRTH_TEXT) ? '<span class="alert">' . ENTRY_DATE_OF_BIRTH_TEXT . '</span>': ''); ?>
<br class="clearBoth" />
<?php
echo zen_draw_hidden_field('dates_email_address', $account2->fields['customers_email_address'], '');
echo zen_draw_hidden_field('customer_id', $_SESSION['customer_id'], '');
?>
<br class="clearBoth" />
</fieldset>

<div class="buttonRow back"><?php echo '<a href="' . zen_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">' . zen_image_button(BUTTON_IMAGE_BACK , BUTTON_BACK_ALT) . '</a>'; ?></div>
<div class="buttonRow forward"><?php echo zen_image_submit(BUTTON_IMAGE_UPDATE , BUTTON_UPDATE_ALT); ?></div>
<br class="clearBoth" />

</form>
</div>
