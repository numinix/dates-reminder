Installation:
1. Upload all files to maintaining the directory structure (rename YOUR_TEMPLATE to your custom template name);
2. Edit includes/languages/english/extra_definitions/account_dates.php with your customized titles and reminder note;
3. Patch your database with the install.sql by copying and pasting the file's contents into ADMIN->TOOLS->INSTALL SQL PATCHES;
4. Set up a DAILY cron job (once per day, no more, no less):
CODE: SELECT ALL
wget �http://www.your_domain.com/catalog/functions_dates_cron.php"


Notes:
This script has been tested and works on Zen Cart v1.3.7 to v1.3.8a;
Overrides includes/templates/template_default/tpl_account_default.php. If you have edited this file previously, please use winmerge or add the following line above the </ul> on line 63:
CODE: SELECT ALL
<?php
//start numinix dates reminder mod
if(NUMINIX_DATES_MOD_REMINDER=='true'){
?>
<li><?php echo ' <a href="' . zen_href_link(FILENAME_ACCOUNT_DATES, '', 'SSL') . '">' . ADD_DATES . '</a>'; ?></li>
<?php
}
//end numinix dates reminder mod
?>