SET @configuration_group_id=0;
SELECT @configuration_group_id=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'Dates Reminder Configuration'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'Dates Reminder Configuration', 'Set Dates Reminder Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Dates Reminder Date', 'NUMINIX_DATES_MOD_REMINDER_DATE', '29', 'How many days before the date, do you want to send a reminder?', @configuration_group_id, 0, NOW(), NULL, NULL);

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Activate Dates Reminder', 'NUMINIX_DATES_MOD_REMINDER', 'false', 'Activate Dates Reminder Module?', @configuration_group_id, 0, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');


CREATE TABLE `dates` (
  `dates_id` int(11) NOT NULL auto_increment,
  `dates_customers_id` int(11) NOT NULL default '0',
  `dates_first_name` varchar(64) NOT NULL default '',
  `dates_last_name` varchar(64) NOT NULL default '',
  `dates_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `dates_email_address` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`dates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2;

